# Callbacks

## `after_failure`

- Allows executing behaviors when a transition requirements are met, but fails to save
- Since callbacks run within transactions, a save failure will cause any record created in the callback to roll back
- You can work around this:

  ```ruby
  class TransitionLog < ActiveRecord::Base
    establish_connection Rails.env.to_sym
  end

  class Vehicle < ActiveRecord::Base
    state_machine do
      after_failure do |vehicle, transition|
        TransitionLog.create(:vehicle => vehicle, :transition => transition)
      end

      ...
    end
  end
  ```

The model establishes a second connection to the database that allows new records to be saved without being affected by rollbacks in the original transaction.
