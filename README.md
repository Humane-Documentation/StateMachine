# StateMachine Gem

* [State Machine](related/state_machines.md)
* [state_machine](related/state_machine/state_machine.md)
* [state](related/state_machine/states.md)
* [transition](related/state_machine/transitions.md)
* [Observers](related/state_machine/observers.md)
* [Actions](related/state_machine/actions.md)
* [Callbacks](related/state_machine/callbacks.md)
* [event](related/state_machine/events.md)
* [Helpers](related/state_machine/helpers.md)
* [ActiveRecord](related/state_machine/scopes.md)

*state_machine* gem adds support for creating state machines for attributes on any
Ruby class. It introduces the various parts of a real state machine, including
states, events, transitions, and callbacks

Represents a state machine for a particular attribute.  State machines
consist of states, events and a set of transitions that define how the
state changes after a particular event is fired.

A state machine will not know all of the possible states for an object
unless they are referenced *somewhere* in the state machine definition.
As a result, any unused states should be defined with the `other_states`
or `state` helper.

![](state_machine.png)



### Features
* Initial states
* Multiple states on a single class
* Namespaced states
* Conditional transitions: before/after/around/failure transition hooks with explicit transition requirements
* Transition callbacks
* State predicates
* State-driven instance/class behavior
* State values of any data type
* Dynamically-generated state values
* Attribute-based event transitions
* Parallel events
* Path analysis
* Inheritance
* GraphViz visualization creator
* Flexible syntax
* Customized state values
* Integration with ActiveModel, ActiveRecord, DataMapper, Mongoid, MongoMapper, and Sequel
* Internationalization

## API
http://rdoc.info/github/pluginaweek/state_machine/master/frames## Detailed Example (Plain Ruby)

## PORO Example
```ruby
class Vehicle
  attr_accessor :seatbelt_on, :time_used, :auto_shop_busy

  state_machine :state, :initial => :parked do
    before_transition :parked => any - :parked, :do => :put_on_seatbelt

    after_transition :on => :crash, :do => :tow
    after_transition :on => :repair, :do => :fix
    after_transition any => :parked do |vehicle, transition|
      vehicle.seatbelt_on = false
    end

    after_failure :on => :ignite, :do => :log_start_failure

    around_transition do |vehicle, transition, block|
      start = Time.now
      block.call
      vehicle.time_used += Time.now - start
    end

    event :park do
      transition [:idling, :first_gear] => :parked
    end

    event :ignite do
      transition :stalled => same, :parked => :idling
    end

    event :idle do
      transition :first_gear => :idling
    end

    event :shift_up do
      transition :idling => :first_gear, :first_gear => :second_gear, :second_gear => :third_gear
    end

    event :shift_down do
      transition :third_gear => :second_gear, :second_gear => :first_gear
    end

    event :crash do
      transition all - [:parked, :stalled] => :stalled, :if => lambda {|vehicle| !vehicle.passed_inspection?}
    end

    event :repair do
      # The first transition that matches the state and passes its conditions
      # will be used
      transition :stalled => :parked, :unless => :auto_shop_busy
      transition :stalled => same
    end

    state :parked do
      def speed
        0
      end
    end

    state :idling, :first_gear do
      def speed
        10
      end
    end

    state all - [:parked, :stalled, :idling] do
      def moving?
        true
      end
    end

    state :parked, :stalled, :idling do
      def moving?
        false
      end
    end
  end

  state_machine :alarm_state, :initial => :active, :namespace => 'alarm' do
    event :enable do
      transition all => :active
    end

    event :disable do
      transition all => :off
    end

    state :active, :value => 1
    state :off, :value => 0
  end

  def initialize
    @seatbelt_on = false
    @time_used = 0
    @auto_shop_busy = true
    super() # IMPORTANT: This *MUST* be called, otherwise states won't get initialized
  end

  def put_on_seatbelt
    @seatbelt_on = true
  end

  def passed_inspection?
    false
  end

  def tow
    # tow the vehicle
  end

  def fix
    # get the vehicle fixed by a mechanic
  end

  def log_start_failure
    # log a failed attempt to start the vehicle
  end
end
```
> `super()` must be called on the `initialize` method. See `StateMachine::MacroMethods` for more about this.

Now you can interact with the state machine defined:
```ruby
vehicle = Vehicle.new           # => #<Vehicle:0xb7cf4eac @state="parked", @seatbelt_on=false>
vehicle.state                   # => "parked"
vehicle.state_name              # => :parked
vehicle.human_state_name        # => "parked"
vehicle.parked?                 # => true
vehicle.can_ignite?             # => true
vehicle.ignite_transition       # => #<StateMachine::Transition attribute=:state event=:ignite from="parked" from_name=:parked to="idling" to_name=:idling>
vehicle.state_events            # => [:ignite]
vehicle.state_transitions       # => [#<StateMachine::Transition attribute=:state event=:ignite from="parked" from_name=:parked to="idling" to_name=:idling>]
vehicle.speed                   # => 0
vehicle.moving?                 # => false

vehicle.ignite                  # => true
vehicle.parked?                 # => false
vehicle.idling?                 # => true
vehicle.speed                   # => 10
vehicle                         # => #<Vehicle:0xb7cf4eac @state="idling", @seatbelt_on=true>

vehicle.shift_up                # => true
vehicle.speed                   # => 10
vehicle.moving?                 # => true
vehicle                         # => #<Vehicle:0xb7cf4eac @state="first_gear", @seatbelt_on=true>

# A generic event helper is available to fire without going through the event's instance method
vehicle.fire_state_event(:shift_up) # => true

# Call state-driven behavior that's undefined for the state raises a NoMethodError
vehicle.speed                   # => NoMethodError: super: no superclass method `speed' for #<Vehicle:0xb7cf4eac>
vehicle                         # => #<Vehicle:0xb7cf4eac @state="second_gear", @seatbelt_on=true>

# The bang (!) operator can raise exceptions if the event fails
vehicle.park!                   # => StateMachine::InvalidTransition: Cannot transition state via :park from :second_gear

# Generic state predicates can raise exceptions if the value does not exist
vehicle.state?(:parked)         # => false
vehicle.state?(:invalid)        # => IndexError: :invalid is an invalid name

# Namespaced machines have uniquely-generated methods
vehicle.alarm_state             # => 1
vehicle.alarm_state_name        # => :active

vehicle.can_disable_alarm?      # => true
vehicle.disable_alarm           # => true
vehicle.alarm_state             # => 0
vehicle.alarm_state_name        # => :off
vehicle.can_enable_alarm?       # => true

vehicle.alarm_off?              # => true
vehicle.alarm_active?           # => false

# Events can be fired in parallel
vehicle.fire_events(:shift_down, :enable_alarm) # => true
vehicle.state_name                              # => :first_gear
vehicle.alarm_state_name                        # => :active

vehicle.fire_events!(:ignite, :enable_alarm)    # => StateMachine::InvalidTransition: Cannot run events in parallel: ignite, enable_alarm

# Human-friendly names can be accessed for states/events
Vehicle.human_state_name(:first_gear)               # => "first gear"
Vehicle.human_alarm_state_name(:active)             # => "active"

Vehicle.human_state_event_name(:shift_down)         # => "shift down"
Vehicle.human_alarm_state_event_name(:enable)       # => "enable"

# States / events can also be references by the string version of their name
Vehicle.human_state_name('first_gear')              # => "first gear"
Vehicle.human_state_event_name('shift_down')        # => "shift down"

# Available transition paths can be analyzed for an object
vehicle.state_paths                                       # => [[#<StateMachine::Transition ...], [#<StateMachine::Transition ...], ...]
vehicle.state_paths.to_states                             # => [:parked, :idling, :first_gear, :stalled, :second_gear, :third_gear]
vehicle.state_paths.events                                # => [:park, :ignite, :shift_up, :idle, :crash, :repair, :shift_down]

# Find all paths that start and end on certain states
vehicle.state_paths(:from => :parked, :to => :first_gear) # => [[
                                                          #       #<StateMachine::Transition attribute=:state event=:ignite from="parked" ...>,
                                                          #       #<StateMachine::Transition attribute=:state event=:shift_up from="idling" ...>
                                                          #    ]]
# Skipping state_machine and writing to attributes directly
vehicle.state = "parked"
vehicle.state                   # => "parked"
vehicle.state_name              # => :parked

# *Note* that the following is not supported (see StateMachine::MacroMethods#state_machine):
# vehicle.state = :parked
```
## ActiveRecord Integration

`StateMachine::Integrations::ActiveRecord` adds support for
* database transactions
* automatically saving the record
* named scopes
* validation errors
* observers


##### *Example*
```ruby
class Vehicle < ActiveRecord::Base
  state_machine :initial => :parked do
    before_transition :parked => any - :parked, :do => :put_on_seatbelt
    after_transition any => :parked do |vehicle, transition|
      vehicle.seatbelt = 'off'
    end
    around_transition :benchmark

    event :ignite do
      transition :parked => :idling
    end

    state :first_gear, :second_gear do
      validates_presence_of :seatbelt_on
    end
  end

  def put_on_seatbelt
    ...
  end

  def benchmark
    ...
    yield
    ...
  end
end

class VehicleObserver < ActiveRecord::Observer
  # Callback for :ignite event *before* the transition is performed
  def before_ignite(vehicle, transition)
    # log message
  end

  # Generic transition callback *after* the transition is performed
  def after_transition(vehicle, transition)
    Audit.log(vehicle, transition)
  end
end
```

```ruby
  class Vehicle < ActiveRecord::Base
    state_machine :initial => :parked do
      event :ignite do
        transition :parked => :idling
      end
    end
  end
```

Sections will use the above class as a reference.

### Examples in Spree
* [Adjustment](https://github.com/spree/spree/blob/master/core/app/models/spree/adjustment.rb)
* [Checkout](https://github.com/spree/spree/blob/master/core/app/models/spree/order/checkout.rb)
* [Inventory Unit](https://github.com/spree/spree/blob/master/core/app/models/spree/inventory_unit.rb)
* [Payment](https://github.com/spree/spree/blob/master/core/app/models/spree/payment.rb)
* [Return Authorization](https://github.com/spree/spree/blob/master/core/app/models/spree/return_authorization.rb)
* [Shipment](https://github.com/spree/spree/blob/master/core/app/models/spree/shipment.rb)

## Usage
#### 1. Prepare your Model
Add a DB field to the model to store the state:
```bash
  # e.g. to create a new `Vehicle` model with a `state` field:  
  rails generate model Vehicle state:string
  rake db:migrate
```

##### Use `StateMachine` Rake task
To quickly generate state machines for any rails class:
```bash
rake state_machine:draw CLASS=Vehicle
```

#### 2. Configure the State Machine in your Model
see all possible Configurations below.


## YARD Documentation
To enable YARD integration, add `state_machine` plugin to your *~/.yard/config*:
```yaml
load_plugins: true
autoload_plugins:
  - state_machine
  ...
```
## Testing
To run core test suite (does **not** contain integration tests):
```bash
bundle install
bundle exec rake test
```
To run integration tests:
```bash
bundle install
rake appraisal:install
rake appraisal:test
```
You can also test a specific version:
```bash
rake appraisal:active_model-3.0.0 test
rake appraisal:active_record-2.0.0 test
```

## Caveats
### Event Methods & Transitions
Overridden event methods won't get invoked when using attribute-based event transitions

### JRuby / Rubinius**
`around_transition` callbacks in ORM integrations won't work on JRuby since it doesn't support continuations
### Factory Girl
Dynamic initial states don't work because of the way `factory_girl`
builds objects.  You can work around this in a few ways:
1. Use a default state that is common across all objects and rely on events to
determine the actual initial state for your object.
2. Assuming you're not using state-driven behavior on initialization, you can
re-initialize states after the fact:

```ruby
# Re-initialize in FactoryGirl
FactoryGirl.define do
  factory :vehicle do
    after_build {|user| user.send(:initialize_state_machines, :dynamic => :force)}
  end
end

# Alternatively re-initialize in your model
class Vehicle < ActiveRecord::Base
  ...
  before_validation :on => :create {|user| user.send(:initialize_state_machines, :dynamic => :force)}
end
```
### Native Extension
By default, StateMachine extends the Ruby core with a `state_machine` method on
`Class`.  All other parts of the library are confined within the `StateMachine`
namespace.  While this isn't wholly necessary, it also doesn't have any performance
impact and makes it truly feel like an extension to the language.  This is very
similar to the way that you'll find `yaml`, `json`, or other libraries adding a
simple method to all objects just by loading the library.

However, if you'd like to avoid having StateMachine add this extension to the
Ruby core:

```ruby
require 'state_machine/core'

class Vehicle
  extend StateMachine::MacroMethods

  state_machine do
    # ...
  end
end
```
If you're using a gem loader like Bundler, you can explicitly indicate which
file to load:
```ruby
# In Gemfile
...
gem 'state_machine', :require => 'state_machine/core'
```

## Internationalization

In Rails 2.2`, any error message that is generated from performing invalid
transitions can be localized.  The following default translations are used:
```ruby
  en:
    activerecord:
      errors:
        messages:
          invalid: "is invalid"
          # %{value} = attribute value, %{state} = Human state name
          invalid_event: "cannot transition when %{state}"
          # %{value} = attribute value, %{event} = Human event name, %{state} = Human current state name
          invalid_transition: "cannot transition via %{event}"
```

Notice that the interpolation syntax is %{key} in Rails 3`.  In Rails 2.x,
the appropriate syntax is {{key}}.

You can override these for a specific model like so:
```ruby
  en:
    activerecord:
      errors:
        models:
          user:
            invalid: "is not valid"
```

In addition to the above, you can also provide translations for the
various states / events in each state machine.  Using the Vehicle example,
state translations will be looked for using the following keys, where
`model_name` = "vehicle", `machine_name` = "state" and `state_name` = "parked":
* <tt>activerecord.state_machines.#{model_name}.#{machine_name}.states.#{state_name}</tt>
* <tt>activerecord.state_machines.#{model_name}.states.#{state_name}</tt>
* <tt>activerecord.state_machines.#{machine_name}.states.#{state_name}</tt>
* <tt>activerecord.state_machines.states.#{state_name}</tt>

Event translations will be looked for using the following keys, where
`model_name` = "vehicle", `machine_name` = "state" and `event_name` = "ignite":
* <tt>activerecord.state_machines.#{model_name}.#{machine_name}.events.#{event_name}</tt>
* <tt>activerecord.state_machines.#{model_name}.events.#{event_name}</tt>
* <tt>activerecord.state_machines.#{machine_name}.events.#{event_name}</tt>
* <tt>activerecord.state_machines.events.#{event_name}</tt>

An example translation configuration might look like so:
```ruby
  es:
    activerecord:
      state_machines:
        states:
          parked: 'estacionado'
        events:
          park: 'estacionarse'
```



## Graphs
### Generating graphs
* You can generate di-graphs based on the events, states, and transitions defined for a state machine using [GraphViz](http://www.graphviz.org)
* This requires that both the `ruby-graphviz` gem and graphviz library be
installed on the system
* For examples of actual images generated, see those under the examples folder

#### Usage
To generate a graph for a specific file / class:
```bash
rake state_machine:draw FILE=vehicle.rb CLASS=Vehicle
```
To save files to a specific path:
```bash
rake state_machine:draw FILE=vehicle.rb CLASS=Vehicle TARGET=files
```
To customize the image format / orientation:
```bash
rake state_machine:draw FILE=vehicle.rb CLASS=Vehicle FORMAT=jpg ORIENTATION=landscape
```
See http://rdoc.info/github/glejeune/Ruby-Graphviz/Constants for the list of
supported image formats.  If resolution is an issue, svg may offer better results.

To generate multiple state machine graphs:
```bash
rake state_machine:draw FILE=vehicle.rb,car.rb CLASS=Vehicle,Car
```
To use human state / event names:
```bash
rake state_machine:draw FILE=vehicle.rb CLASS=Vehicle HUMAN_NAMES=true
```

**Note** that this will generate a different file for every state machine defined
in the class.  The generated files will use an output filename of the format
`#{class_name}_#{machine_name}.#{format}`.

### Interactive graphs
Jean Bovet's [Visual Automata Simulator](http://www.cs.usfca.edu/~jbovet/vas.html)
is a great tool for "simulating, visualizing and transforming finite state
automata and Turing Machines".  It can help in the creation of states and events
for your models.  It is cross-platform, written in Java.
