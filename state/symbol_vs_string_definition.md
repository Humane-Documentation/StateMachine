# `state` / `event`

## Symbol Vs. String Definition

- In all examples you'll notice states and events are referenced as _symbols_ as that's the best practice. However, you can also use numbers or strings:

  ```ruby
  class Vehicle
  state_machine :initial => 'parked' do
    event 'ignite' do
      transition 'parked' => 'idling'
    end
    ...
  end
  end
  ```

- Type used for referencing states and events in the machine definition **must be consistent** or you'll encounter this error:

```ruby
class Vehicle
  state_machine do
    event :ignite do
      transition :parked => 'idling'
    end
  end
end

# => ArgumentError: "idling" state defined as String, :parked defined as Symbol; all states must be consistent
```

- Consistency is only required within the definition. e.g. if a machine's helper methods are called with input from external sources, such as a web form, `StateMachine` will still map that input to a String or Symbol:

```ruby
class Vehicle
  state_machine :initial => :parked do
    event :ignite do
      transition :parked => :idling
    end
  end
end

v = Vehicle.new     # => #<Vehicle:0xb71da5f8 @state="parked">
v.state?('parked')  # => true
v.state?(:parked)   # => true
```
