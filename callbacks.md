# Callbacks

Callbacks are supported for hooking before and after every possible transition in the machine

- Callbacks are executed **ONLY** within the context of an event
- Behave in the same way that other ActiveRecord callbacks behave
- Object involved in the transition is passed in as an argument

```ruby
  class Vehicle < ActiveRecord::Base
    state_machine :initial => :parked do
      before_transition any => :idling do |vehicle|
        vehicle.put_on_seatbelt
      end

      before_transition do |vehicle, transition|
        # log message
      end

      event :ignite do
        transition :parked => :idling
      end
    end

    def put_on_seatbelt
      ...
    end
  end
```

- The transition can be accessed by defining additional arguments in the callback block

## Callback Order

- (-) save
- (-) begin transaction (if enabled)
- (1) _before_transition_
- (-) valid
- (2) before_validation
- (-) validate
- (3) after_validation
- (4) before_save
- (5) before_create
- (-) create
- (6) after_create
- (7) after_save
- (8) _after_transition_
- (-) end transaction (if enabled)
- (9) after_commit

## Callbacks on Object Creation

- If a class has an initial state when created, any callbacks that would normally get executed when
the object enters that state will **NOT** get triggered. For example:

```ruby
  class Vehicle
    state_machine :initial => :parked do
      after_transition all => :parked do
        raise ArgumentError
      end
      ...
    end
  end

  vehicle = Vehicle.new   # => #<Vehicle id: 1, state: "parked">
  vehicle.save            # => true (So no exception raised)
```

To rectify this, either:

- Use a `before :create` or equivalent hook:

```ruby
    class Vehicle
      before :create, :track_initial_transition

      state_machine do
        ...
      end
    end
```

- Set an initial state and use the correct event to create the object with the proper state,
resulting in callbacks being triggered and the object getting persisted (`:pending` state is stored
as `nil`):

```ruby
    class Vehicle
       state_machine :initial => :pending
        after_transition :pending => :parked, :do => :track_initial_transition

        event :park do
          transition :pending => :parked
        end

        state :pending, :value => nil
      end
    end

    vehicle = Vehicle.new
    vehicle.park
```

- Use a default event attribute that will automatically trigger when the configured action gets run:

```ruby
    class Vehicle < ActiveRecord::Base
      state_machine :initial => :pending
        after_transition :pending => :parked, :do => :track_initial_transition

        event :park do
          transition :pending => :parked
        end

        state :pending, :value => nil
      end

      def initialize(*)
        super
        self.state_event = 'park'
      end
    end

    vehicle = Vehicle.new
    vehicle.save
```

## Canceling Callbacks

Done by throwing `:halt` at any point during a callback:

```ruby
  ...
  throw :halt
  ...
```

- If a `before` callback halts the chain, the associated transition and all later callbacks are canceled
- If an `after` callback halts the chain, later callbacks are canceled, but the transition is still
successful
- Same applies to `around` callbacks except that any callback that doesn't yield will throw in a
`:halt`. Code executed after the yield will behave in the same way as `after` callbacks
- if a `before` callback fails and the _bang_ version of an event was invoked, an exception will be
raised instead of returning `false`:

  ```ruby
  class Vehicle
    state_machine :initial => :parked do
      before_transition any => :idling, :do => lambda {|vehicle| throw :halt}
      ...
    end
  end

  vehicle = Vehicle.new
  vehicle.park        # => false
  vehicle.park!       # => StateMachines::InvalidTransition: Cannot transition state via :park from "idling"
  ```
