# `transition`

## Transactions
Every transition is wrapped within a transaction to ensure *changes during transition callbacks can
be rolled back* in case of failure.

For example:
```ruby
  class Message < ActiveRecord::Base
  end

  Vehicle.state_machine do
    before_transition do |vehicle, transition|
      Message.create(:content => transition.inspect)
      false
    end
  end

  vehicle = Vehicle.create      # => #<Vehicle id: 1, name: nil, state: "parked">
  vehicle.ignite                # => false
  Message.count                 # => 0
```
* Typical transaction roll back:
  - A `before_` callback(s) halts the callback chain
  - Attempts to save the record failed
* If an `after_` callback halts the chain, the previous result
still applies and the transaction is **not** rolled back
* To turn off transactions:
```ruby
  class Vehicle < ActiveRecord::Base
    state_machine :initial => :parked, :use_transactions => false do
      ...
    end
  end
```
