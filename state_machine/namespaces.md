# `state_machine`

## Namespaces

When a namespace is configured for a state machine, the name provided
will be used in generating the instance methods for interacting with
states/events in the machine.  This is particularly useful when a class
has multiple state machines and it would be difficult to differentiate
between the various states / events.

For example,
```ruby
  class Vehicle
    state_machine :heater_state, :initial => :off, :namespace => 'heater' do
      event :turn_on do
        transition all => :on
      end

      event :turn_off do
        transition all => :off
      end
    end

    state_machine :alarm_state, :initial => :active, :namespace => 'alarm' do
      event :turn_on do
        transition all => :active
      end

      event :turn_off do
        transition all => :off
      end
    end
  end
```
The above class defines two state machines: `heater_state` and `alarm_state`.
For the `heater_state` machine, the following methods are generated since
it's namespaced by `heater`:
* `can_turn_on_heater?`
* `turn_on_heater`
* ...
* `can_turn_off_heater?`
* `turn_off_heater`
* ..
* `heater_off?`
* `heater_on?`

As shown, each method is unique to the state machine so that the states
and events don't conflict.  The same goes for the `alarm_state` machine:
* `can_turn_on_alarm?`
* `turn_on_alarm`
* ...
* `can_turn_off_alarm?`
* `turn_off_alarm`
* ..
* `alarm_active?`
* `alarm_off?`
